#!/usr/bin/python3
"""
    Script to be ran from CRON to backup the database and filesystem
        - both are sent to a Linode bucket
        - database backups are sent nightly
        - filesystem is sent once a week
    Required args: password to encrypt backup, database name and bucket address
"""

import os
import sys
import subprocess as sb
from datetime import date as dt

# Initialize credentials and necessary info
date = dt.today().strftime('%Y-%m-%d')
pwd = sys.argv[1]
db = sys.argv[2]
bucket = sys.argv[3]

# Database backup
db_file = f'/backup/absorto-wiki-{date}.sql.7z'
sb.run(f'mysqldump -u root {db} | 7z a -si {db_file} -p {pwd}')
sb.run(f'linode-cli obj put {db_file} {bucket}')

# File system backup
if dt.today().strftime('%a') == 'Sat':
    archive = f'/backup/absorto-wiki-{date}.tar.7z'
    sb.run(f'tar cf - -X /var/www/absortowiki/backup-exclusions \
           /var/www/absortowiki | 7z a -si {archive} -p {pwd}')
    sb.run(f'linode-cli obj put {archive} {bucket}')

    # Remove backup files older than two weeks
    for folder, subs, files in os.walk('/backup'):
        for filename in files:
            file_path = os.path.abspath(os.path.join(folder, filename))
            if os.path.getmtime(file_path) > 1209600:
                os.remove(file_path)
